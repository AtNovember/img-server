function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        console.log("200 - 300");
        return response
    } else {
        var error = new Error(response.statusText)
        console.log("err", error);
        error.response = response
        throw error
    }
}

function parseJSON(response) {
    return response.json()
}

var image;

function uploadFile() {

    var input = document.querySelector('input[type="file"]')

    var data = new FormData()
    data.append('image', input.files[0])

    fetch('/uploads', {
            method: 'POST',
            body: data
        })
        .then(checkStatus)
        .then(parseJSON)
        .then(function(response) {
            console.log('request succeeded with JSON response', response)
            console.log('image', response.image);


            input = document.querySelector('input[type="file"]');
            var newInput = document.createElement('input');
                newInput.type = "text";
                newInput.name = "image";
                newInput.value = response.image;
                // newInput.innerHTML = response.image;
               input.parentNode.replaceChild(newInput, input);

            // return response.text()
            return response.image;
        })
        .then(function(image) {
            console.log('got text', image)
        })
        .catch(function(error) {
            console.log('request failed', error)
        })


};
